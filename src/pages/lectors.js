import React from 'react'
import Layout from '../layouts/main-layout'

import { MainApi } from '../api/MainApi'
import LectorCourses from '../components/LectorCourses'
import Footer from '../components/footer'

class Lectors extends React.Component {
  state = {
    list: [],
  }

  getLectorsList = () => {
    MainApi.getLectorsList().then((response) => {
      this.setState({ list: response })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }

  render() {
    let { list } = this.state
    return (
      <Layout>
        <div className="content container lectors">
          <h6 className="heading">Лекторы</h6>
          {list.map((item, i) => (
            <div key={i} className="media">
              <h6 className="inner">{item.name}</h6>
              <div className="row float-reverse">
                <div className="col-sm-9 no-padding-left">
                  <p>{this.cleanHtml(item.text)}</p>
                  <LectorCourses lector={item.id}/>
                </div>
                <div className="col-sm-3 img-lector">
                  <div className="user-img-upload">
                    <img src={item.image} alt="Валерий Сторожук"/>
                  </div>
                </div>
              </div>
            </div>
          ))}

          <Footer/>
        </div>
      </Layout>
    )
  }

  componentDidMount() {
    this.getLectorsList()
  }
}

export default Lectors
