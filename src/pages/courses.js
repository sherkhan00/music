import React from 'react'
import Layout from '../layouts/main-layout'
import {BrowserRouter as Router, Route, Link} from "react-router-dom";

import {MainApi} from '../api/MainApi'
import LectorThumbnail from '../components/LectorThumbnail'
import Footer from '../components/footer'
import Helmet from 'react-helmet'
import ReviewNote from '../components/ReviewNote'
import Course from '../components/Course'
import {HomeApi} from '../api/HomeApi'

class Courses extends React.Component {
    state = {
        list: [],
        epoches: [],
        selectedEpoches: 0,
    }

    getCourseList = () => {
        MainApi.getCourseList().then((response) => {
            if (this.state.selectedEpoches !== 0)
                response = response.filter(item => item.epoch === this.state.selectedEpoches)

            let list = []
            let childlist = []
            let index = 0
            response.map((item, i) => {
                    childlist.push(item)
                    list[index] = childlist
                    let indexInner = i + 1
                    if (indexInner % 3 === 0) {
                        index++
                        childlist = []
                    }
                },
            )
            this.setState({list: list})
        })
    }

    getEpochesList = () => {
        MainApi.getEpochesList().then((response) => {
            this.setState({epoches: response})
        })
    }

    onClickEpoches = (id) => {
        this.setState({selectedEpoches: id})
        this.getCourseList()
    }

    cleanHtml(html) {
        return html.replace(/<(?:.|\n)*?>/gm, '')
    }

    render() {
        let {list, epoches, selectedEpoches} = this.state
        let {match} = this.props
        return (
            <div>
                <Route path={`${match.url}/:courseId`} component={Course}/>
                <Route
                    exact
                    path={match.url}
                    render={() => (
                        <Layout>
                            <div className="content container review">
                                <div className="section-title-inner">
                                    <h6>КУРСЫ</h6>
                                    <ul className="heading-list">
                                        <li key='0' onClick={() => this.onClickEpoches(0)}><a href="#" className="active">Все</a>
                                        </li>
                                        {epoches.map((item, i) => (
                                            <li key={i} onClick={() => this.onClickEpoches(item.id)}><a href="#">{item.title}</a>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                                {list.map((item, i) => (
                                    <div key={i} className="row item-blocks">
                                        <div className="media blocks">
                                            {item.map((itemInner, i2) => (
                                                <div key={i2} className="col-sm-4">
                                                    <a className="inform" href={`/courses/${itemInner.id}`}>
                                                        <div className="block-red item">
                                                            <div className="image-src"><img src={itemInner.image} alt=""/></div>
                                                            <div className="information">
                                                                <div className="crew"></div>
                                                                <p className="date">{itemInner.number_lectures} Лекций</p>
                                                                <h5 className="title">{itemInner.title}</h5>
                                                                <LectorThumbnail small={true} id={itemInner.lecturer}/>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                ))}

                                <div className="clear"></div>
                                <Footer/>
                            </div>
                        </Layout>
                    )}
                />
            </div>

        )
    }

    componentDidMount() {
        this.getCourseList()
        this.getEpochesList()
    }
}

export default Courses
