import 'regenerator-runtime/runtime'

const API_URL = `http://31.192.109.24/api/`

export class HomeApi {
    static unifetch = (URL) => {
        return fetch(API_URL + URL).then(response => response.json())
            .then((responseJson) => {
                return responseJson
            })
    }

    static getPopularsList() {
        try {
            return this.unifetch(`populars/?limit=3`).then((responseJson) => {
                return responseJson.results
            })
        } catch (error) {
            console.log('Error when call API (getPopularsList): ' + error.message)
        }
    }


    static getPopularData(id) {
        try {
            return this.unifetch(`populars/${id}/`).then((responseJson) => {
                return responseJson.content
            })

        } catch (error) {
            console.log('Error when call API (getPopularsList): ' + error.message)
        }
    }

    static getTrackList() {
        try {
            return this.unifetch(`tracklist/?limit=3`).then((responseJson) => {
                return responseJson.results
            })

        } catch (error) {
            console.log('Error when call API (onLogin): ' + error.message)
        }
    }

    static getInterviewsList() {
        try {
            return this.unifetch(`interviews/?limit=3`).then((responseJson) => {
                return responseJson.results
            })
        } catch (error) {
            console.log('Error when call API (onLogin): ' + error.message)
        }
    }

    static getSlideData() {
        try {
            return this.unifetch(`critiques/?limit=3`).then((responseJson) => {
                return responseJson.results
            })
        } catch (error) {
            console.log('Error when call API (getSlideData): ' + error.message)
        }
    }
}



