import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title="Page"
      meta={[
        { name: 'charSet', content: 'UTF-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'description', content: 'Page desc' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <div>
      {children}
    </div>
  </div>
)


export default Layout
