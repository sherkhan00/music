import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/header'

const CourseList = ({ children, data }) => (
  <div className="row item-blocks">
    <div className="media blocks">
      {children()}
    </div>
  </div>
)


export default CourseList
