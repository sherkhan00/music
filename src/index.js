import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
//import {Router, Route, Switch, IndexRoute} from 'react-router'
import {Switch, BrowserRouter as Router, Route, Link} from "react-router-dom";

import App from './App';
import Home from './pages/home';
import Courses from './pages/courses'
import Course from "./components/Course";
import Lectors from "./pages/lectors";
import Review from "./pages/review";
import VideoBlog from "./pages/video-blog";
import Triko from "./pages/triko";
import Others from "./pages/others";
//const history = createBrowserHistory()

const Navigation = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/courses" component={Courses}/>
            <Route path="/lectors" component={Lectors}/>
            <Route path="/review" component={Review}/>
            <Route path="/video-blog" component={VideoBlog}/>
            <Route path="/triko" component={Triko}/>
            <Route path="/others" component={Others}/>
        </Switch>
    </Router>
);

ReactDOM.render(
    <Navigation/>,
    document.getElementById('root'));
registerServiceWorker();
