import React from 'react'

import { HomeApi } from '../api/HomeApi'
import LectorThumbnail from './LectorThumbnail'
import { MainApi } from '../api/MainApi'

class Popular extends React.Component {
  state = {
    content: {},
    loading: true,
  }

  getPopularData = () => {
    HomeApi.getPopularData(this.props.id).then((response) => {
      this.setState({ content: response, loading: false })
    })
  }

  cleanHtml(html) {
    return html.replace(/<(?:.|\n)*?>/gm, '')
  }


  render() {
    if (this.state.loading) {
      return <p></p>
    }
    let { content } = this.state
    let image = 'http://31.192.109.24' + content.image
    let dateOr = content.date ? MainApi.getDate(content.date) : content.number_lectures + ' лекции'
    let contentType = this.props.contentType

    return (
      <div className="col-sm-4">
        <a className="inform" href={contentType === 12 ? 'courses/'+content.id : '#'}>
          <div className="block-red item">
            <div className="image-src"><img src={image}
                                            alt=""/></div>
            <div className="information">
              <div className="crew"></div>
              <p className="date">{dateOr}</p>
              <h5 className="title">{content.title}</h5>

              {contentType === 12 &&
              <LectorThumbnail small={true} id={content.lecturer}/>
              }

              {contentType === 16 &&
              <div>
                <p dangerouslySetInnerHTML={{ __html: this.cleanHtml(content.text_short) }}></p>
                < p className='autor'>{content.author_name}</p>
              </div>
              }

              {contentType === 21 &&
              <div>
                <p dangerouslySetInnerHTML={{ __html: this.cleanHtml(content.text_short) }}></p>
                < img src='/static/img/icons/icon-cinema.png' alt='' className='img-pic'/>
              </div>
              }
            </div>
          </div>
        </a>
      </div>
    )
  }

  componentDidMount() {
    this.getPopularData()
  }
}

export default Popular
