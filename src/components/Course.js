import React from 'react'
import Layout from '../layouts/main-layout'

import {MainApi} from '../api/MainApi'
import LectorThumbnail from './LectorThumbnail'
import Footer from './footer'
import Helmet from "react-helmet";


class Course extends React.Component {
    state = {
        id: '',
        course: [],
        loading: true,
        lectures: {count: '0', results: []},
        activeLecture: 0,
        lectureContent: {title: '', text: '', duration: '0'},
        tracks: [],
    }


    getCourse = () => {
        let id = this.props.match.params.courseId
        MainApi.getCourse(id).then((response) => {
            this.setState({course: response, id: id})
            this.getCourseLectures()
            this.getCourseTracks()
        })
    }

    // лЕКЦИИ
    getCourseLectures = () => {
        MainApi.getCourseLectures(this.state.id).then((response) => {
            if (response.results[0])
                this.setState({lectures: response, lectureContent: response.results[0], activeLecture: 1})
        })
    }

    // Tracks
    getCourseTracks = () => {
        MainApi.getCourseTracks(this.state.id).then((response) => {
            this.setState({tracks: response, loading: false})
        })
    }

    cleanHtml(html) {
        return html.replace(/<(?:.|\n)*?>/gm, '')
    }

    onClickLecture = (e, lecture, index) => {
        e.preventDefault()
        this.setState({lectureContent: lecture, activeLecture: index + 1})
    }

    render() {
        let {course, loading, lectures, lectureContent, activeLecture, tracks} = this.state
        if (loading)
            return <p></p>

        console.log('activeLecture', activeLecture)

        return (
            <Layout>
                <div className="content container course">
                    <h2>{course.title}</h2>
                    <a className="permalink" href="/courses">К списку курсов </a>
                    <div className="row clear">
                        <section className="about">
                            <div className="col-md-4 mr">
                                <div className="media-img media blocks">
                                    <a className="inform" href="#">
                                        <div className="block-red item white">
                                            <div className="image-src"><img src={course.image} alt=""/></div>
                                            <div className="information">
                                                <div className="crew"></div>
                                                {/*<h5 className="title">1833-1897</h5>*/}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div className="media-body block col-md-5"
                                 dangerouslySetInnerHTML={{__html: this.cleanHtml(course.lecturer.text)}}>
                            </div>
                            <div className="col-sm-3 block autor-subject">
                                <div className="user-img-upload">
                                    <LectorThumbnail small={false} id={course.lecturer.id}/>
                                </div>
                                <p><strong>{course.lecturer.name}</strong></p>
                                <p></p>
                            </div>
                        </section>
                    </div>
                </div>
                <div className="course content container">
                    <div className="row lections-inner">
                        <div className="col-sm-4 lections">
                            <div className="heading">
                                <h3><big>{lectures.count}</big> лекций</h3>
                            </div>
                            <ol>
                                {lectures.results.map((item, i) => (
                                    <li key={i} className={activeLecture === i + 1 ? 'active' : null}>
                                        <a href="#" onClick={(e) => this.onClickLecture(e, item, i)}>{item.title}</a>
                                    </li>
                                ))}
                            </ol>
                        </div>
                        <div className="col-sm-8 lection-info">
                            <div className="heading"><img src="/static/img/icons/time.png"
                                                          alt=""/><span>{lectureContent.duration} минуты</span></div>

                            <div className="owl-carousel-videos owl-style inner">
                                {lectures.results.map((item, i) => (
                                    <div key={i} className="slide">
                                        <a className="owl-video"
                                           href="https://www.youtube.com/embed/dzNvk80XY9s"></a>
                                        <div className="head-title">
                                            <h5>{activeLecture}/{lectures.count}. {item.title}</h5>
                                            <p className="time">{item.duration} минуты</p>
                                        </div>

                                        <p className="clear"> {item.text}</p>
                                    </div>
                                ))}
                            </div>
                            {/*}
                            <div className="owl-carousel-videos owl-style inner">
                                <div className="slide">
                                    <a className="owl-video"
                                       href="https://www.youtube.com/watch?v=0Cvzt9crhBs&list=RD0Cvzt9crhBs&start_radio=1"></a>
                                    <div className="head-title">
                                        <h5>1/10. Брамс. Один из самых исполняемых композиторов</h5>
                                        <p className="time">4 минуты</p>
                                    </div>

                                    <p className="clear">В современном концертном репертуаре инструментальная музыка
                                        Брамса сохранилась
                                        целиком — все симфонии, все инструментальные концерты, все камерные ансамбли и
                                        фортепианные
                                        пьесы. Такое бывает крайне редко. В чем особенность творчества Брамса и чем он
                                        отличается от
                                        остальных романтиков? Начинаем разбираться. </p>
                                </div>
                                <div className="slide">
                                    <a className="owl-video"
                                       href="https://www.youtube.com/watch?v=0Cvzt9crhBs&list=RD0Cvzt9crhBs&start_radio=1"></a>
                                    <div className="head-title">
                                        <h5>1/10. Брамс. Один из самых исполняемых композиторов</h5>
                                        <p className="time">4 минуты</p>
                                    </div>
                                    <p className="clear">В современном концертном репертуаре инструментальная музыка
                                        Брамса сохранилась
                                        целиком — все симфонии, все инструментальные концерты, все камерные ансамбли и
                                        фортепианные
                                        пьесы. Такое бывает крайне редко. В чем особенность творчества Брамса и чем он
                                        отличается от
                                        остальных романтиков? Начинаем разбираться. </p>
                                </div>
                            </div>*/}
                        </div>
                    </div>
                </div>
                <div className="container content course">
                    <section className="listen clearfix">
                        <h4>Послушать</h4>

                        {tracks.map((item, i) => (
                            <div key={i} className="col-sm-6 video">
                                <h3>{item.title}</h3>
                                <iframe src={item.track} frameBorder="0" allow="autoplay; encrypted-media"
                                        allowFullScreen></iframe>
                            </div>
                        ))}

                        {/*<div className="col-sm-12 video video-center">
              <h3>Брамс. Соната для фортепиано № 2 фа-диез минор</h3>
              <iframe src="https://www.youtube.com/embed/dzNvk80XY9s" frameBorder="0" allow="autoplay; encrypted-media"
                      allowFullScreen></iframe>
            </div>*/}
                    </section>
                    {/*}
                    <section className="live clear">
                        <h4>Брамс / Live</h4>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="box">
                                    <h6>3 сентября 2018 / 19:00</h6>
                                    <p>Госоркестр им. Светланова
                                        Дирижер — Владимир Юровский
                                        Борис Березовский (фортепиано)</p>
                                    <p><i>Брамс. Концерт №1 для фортепиано
                                        Симфония №4.</i></p>
                                    <p>Концертный зал Чайковского</p>
                                    <a className="vector">
                                    </a>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="box">
                                    <h6>8 сентября 2018 / 19:00</h6>
                                    <p>Российский национальный оркестр
                                        Дирижер — Михаил Плетнев
                                        Борис Березовский (фортепиано)
                                        Сергей Крылов (скрипка)</p>
                                    <p><i>Мендельсон. Концерт для фортепиано и скрипки с оркестром
                                        Брамс. Симфония №2.</i></p>
                                    <p>Концертный зал Чайковского</p>
                                    <a className="vector">

                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="box">
                                    <h6>3 сентября 2018 / 19:00</h6>
                                    <p>Госоркестр им. Светланова
                                        Дирижер — Владимир Юровский
                                        Борис Березовский (фортепиано)</p>
                                    <p><i>Брамс. Концерт №1 для фортепиано
                                        Симфония №4.</i></p>
                                    <p>Концертный зал Чайковского</p>
                                    <a className="vector">

                                    </a>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="box">
                                    <h6>8 сентября 2018 / 19:00</h6>
                                    <p>Российский национальный оркестр
                                        Дирижер — Михаил Плетнев
                                        Борис Березовский (фортепиано)
                                        Сергей Крылов (скрипка)</p>
                                    <p><i>Мендельсон. Концерт для фортепиано и скрипки с оркестром
                                        Брамс. Симфония №2.</i></p>
                                    <p>Концертный зал Чайковского</p>
                                    <a className="vector">

                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>*/}
                    <Footer/>

                </div>
            </Layout>
        )
    }

    componentDidMount() {
        this.getCourse()
    }
}

export default Course
