import React from 'react'

import { MainApi } from '../api/MainApi'

class LectorThumbnail extends React.Component {
  state = {
    lector: [],
  }

  getLecturerImage = () => {
    MainApi.getLecturer(this.props.id).then((response) => {
      this.setState({ lector: response })
    })
  }

  render() {
    let { lector } = this.state
    let width = this.props.small ? '66px' : ''
    return (
      <img style={{ width: width }} src={lector.image} alt="" className="img-pic"/>
    )
  }

  componentDidMount() {
    this.getLecturerImage()
  }
}

export default LectorThumbnail
