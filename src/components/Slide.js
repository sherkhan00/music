import React from 'react'

import {MainApi} from '../api/MainApi'
import {HomeApi} from '../api/HomeApi'
import Helmet from 'react-helmet'

class Slide extends React.Component {
    cleanHtml(html) {
        return html.replace(/<(?:.|\n)*?>/gm, '')
    }

    getElementStyle(i) {
        if (i === 0) {
            return 'elmnt-one'
        } else if (i === 1) {
            return 'elmnt-two'
        } else if (i === 2) {
            return 'elmnt-three'
        } else if (i === 3) {
            return 'elmnt-one'
        } else if (i === 4) {
            return 'elmnt-two'
        } else if (i === 5) {
            return 'elmnt-three'
        }
    }


    render() {
        let slide = this.props.slide
        return (
            <div>
                <div id="owl-carousel">
                    {slide.map((item, i) => (
                        <div key={i} style={{background: `url(${item.image})`}} className="info-mob">
                            <div className="wrap">
                                <p className="review">Рецензия</p>
                                <h2 className="hed">{item.title}</h2>
                                <p className="desc" dangerouslySetInnerHTML={{__html: this.cleanHtml(item.text_short)}}></p>
                                <div className="autor">
                                    <img src={item.author_image} alt="" className="user-pic"/>
                                    <span dangerouslySetInnerHTML={{__html: item.author_name}}></span>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>

                <div className="stage">
                    <div className="over-left"></div>
                    <div className="hdi">
                        <div className="over-right"></div>
                    </div>
                    <div id="SLDE-ONE" className="sldr slider">
                        <ul className="wrp animate">

                            {slide.map((item, i) => (
                                <li key={i} className={this.getElementStyle(i)}>
                                    <div className="info">
                                        <p className="review">Рецензия</p>
                                        <h2 dangerouslySetInnerHTML={{__html: item.title}}></h2>
                                        <p className="desc"
                                           dangerouslySetInnerHTML={{__html: this.cleanHtml(item.text_short)}}></p>
                                        <div className="autor">
                                            <img src={item.author_image} alt=""
                                                 className="user-pic"/><span
                                            dangerouslySetInnerHTML={{__html: item.author_name}}></span>
                                        </div>
                                    </div>
                                    <div className="skew">
                                        <div className="wrap"><img src={item.image} alt=""/>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>

                    <div className="clear"></div>
                    <button className="sldr-prv sldr-nav prev"></button>
                    <button className="sldr-nxt sldr-nav next"></button>
                    <ul className="selectors">
                        <li className="focalPoint"></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        )
    }


}

export default Slide
